# archlabs-keyring

To trust a new key:

Add it to `archlabs-trusted` then run
```
rm archlabs.gpg
for k in $(awk -F':' '{print $1}' archlabs-trusted); do gpg --armor --export "$k" >> archlabs.gpg; done
```

To revoke a key:

Add it to `archlabs-revoked`
